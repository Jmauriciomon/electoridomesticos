
package proyecto;

public abstract class Proyecto {
 
        private String nombre;
	private double precio;
	private String color;
	private String marca;	
	
	public String getNombre() {
		return this.nombre;
        }
        
        public double getPrecio() {
		return this.precio;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public String getMarca() {
		return this.marca;
	}
	
    }
